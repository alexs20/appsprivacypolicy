# WakeUp Touch L Privacy Policy

This application uses following sensitive permissions:

** READ_PHONE_STATE ** - to disable the sensor monitor at call time. No additional data captured, used or stored using this permission.

** BIND_DEVICE_ADMIN ** - to lock and turn off the display. No additional data captured, used or stored using this permission.
